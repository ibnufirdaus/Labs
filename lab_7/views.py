from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os

response = {}
csui_helper = CSUIhelper()

def index(request):
    #mendapatkan daftar mahasiswa dari api cs
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'

    page = request.GET.get('page', 1)
    # membuat paginasi
    paginate_data = paginate(page, mahasiswa_list)
    mahasiswa = paginate_data['data']
    page_range = paginate_data['page_range']

    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list, "page_range": page_range}

    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def get_friend_list(request):
    if request.method == 'GET':
        friend_list = Friend.objects.all()
        data = serializers.serialize('json', friend_list)
        return HttpResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

def paginate(request, mahasiswa_list):
    # Paginator
    paginator = Paginator(mahasiswa_list, 10)

    try:
        mahasiswa = paginator.page(request)
    except PageNotAnInteger:
        mahasiswa = paginator.page(1)
    except EmptyPage:
        mahasiswa = paginator.page(paginator.num_pages)

    index = mahasiswa.number - 1

    max_index = len(paginator.page_range)

    start_index = index if index >= 10 else 0
    end_index = 10 if index < max_index - 10 else max_index

    page_range = list(paginator.page_range)[start_index:end_index]

    data_paginate = {'data': mahasiswa, 'page_range': page_range}
    return data_paginate

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/friend-list')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm__iexact=npm).exists()
    }
    return JsonResponse(data)
