  // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '561215560936850',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)

      FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            render(true);
        }
        else if (response.status === 'not_authorized') {
            render(false);
        }
        else {
            render(false);
        }
    });

    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
                '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.gender[0].toUpperCase() + user.gender.substring(1,user.gender.length) + '</h3>' +
              '<h4>' + user.email + '</h4>' +
              '<button class="logout btn-danger" onclick="facebookLogout()">Logout</button>' +
            '</div>' +
          '</div>' +
          '<form class="form-horizontal" role="form">' +
          '<div class="postForm">' +
          '<textarea id="postInput" class="form-control" type="text" class="post" placeholder="Ketik Status Anda" ></textarea>' +
          '</form>' +
          '<button class="postStatus btn-success" onclick="postStatus()">Post ke Facebook</button>' +
          '</div>'

        );

        // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
        $('.status').append(
            '<div>' +
            '<h1>Feeds</h1>' +
            '</div>'
        );
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message) {
              $('.status').append(
                '<div class="feed">' +
                  '<h1 class="feedMessage">' + value.message + '</h1>' +
                  '<p style="font-size: 18px"><em><strong>' + 'posted ' + moment(value.created_time).fromNow() + '</strong></em></p>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<button class="login btn-primary" onclick="facebookLogin()">Login with Facebook</button>');
      $(".status").html("");
    }
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
       console.log(response);
       render(true);
     }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
      FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          render(false);
          window.location.reload();
        }
     });

  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
       FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(168).height(168)', 'GET', function(response){
            console.log(response);
            fun(response);
          });
        }
    });
  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
      FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            FB.api('/me/posts', 'GET', function (response) {
                console.log(response);
                if (response && !response.error) {
                    /* handle the result */
                    fun(response);
                }
                else {
                    alert("Something went wrong");
                }
            });
        }
    });
  };

  const postFeed = (message) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api('/me/feed', 'POST', {message: message});
    render(true);

};

const postStatus = () => {
    const message = $('#postInput').val();
    $('#postInput').val("");
    postFeed(message);
    window.location.reload();
};
